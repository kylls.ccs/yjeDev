---
title: "Multi-Mounting type Monitor"
date: "2021/10/12"
# excerpt: "Arbor"
excerpt: "Panel mount | Wall mount | VESA mount | There are using standard mounting-type design, and we can base on your requests to redesign the mounting method for you."
cover_image: "/image/posts/products/panel_mount/AD065-FR.jpg"
website: "www.aaeon.com"
---

<img src='../../image/partners/ARBOR.png' />

## Advantech

> www.arbor.com
