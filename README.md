1. Clone the Material-ui with NextJS

```bash
git clone  https://gitlab.com/kylls.ccs/yjeDev.git [your project name]
```

2. Show the NextJS version

```bach
npx next -v
```

3. Show the npm version

```bach
npm -v
```
