import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Link from "../src/Link";
import { Box } from "@mui/material";

export default function IndexCards(props) {
    return (
        <Link href="/products">
            <Card sx={{ width: "auto", height: "auto" }}>
                <CardActionArea>
                    <Box sx={{ display: "flex" }}>
                        <CardMedia
                            component="img"
                            sx={{ width: 1 / 15, height: 1 / 1.5 }}
                            image={props.post.frontmatter.cover_image}
                            alt={props.post.frontmatter.title}
                        />
                        <CardContent sx={{ width: "100" }}>
                            <Typography
                                gutterBottom
                                variant="h5"
                                component="div"
                            >
                                {props.post.frontmatter.title}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {props.post.frontmatter.excerpt}
                            </Typography>
                        </CardContent>
                    </Box>
                </CardActionArea>
            </Card>{" "}
        </Link>
    );
}

// props default vaule
// IndexCards.defaultProps = {
//     linkto: "/product",
// };
