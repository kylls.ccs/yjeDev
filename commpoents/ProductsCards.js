import * as React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { CardActionArea } from "@mui/material";
import Link from "../src/Link";
import { Box } from "@mui/material";
import { Button } from "@mui/material";

export default function ProductsCards(props) {
    return (
        <Link href="/products">
            <Card sx={{ width: "auto", height: "auto" }}>
                <CardActionArea>
                    <Box sx={{ display: "flex" }}>
                        <CardMedia
                            component="img"
                            sx={{ width: 1 / 15, height: 1 / 1.5 }}
                            image={props.main_post.frontmatter.cover_image}
                            alt={props.main_post.frontmatter.title}
                        />
                        <CardContent sx={{ width: "100" }}>
                            <Typography
                                gutterBottom
                                variant="h5"
                                component="div"
                            >
                                {props.main_post.frontmatter.title}
                            </Typography>
                            <Typography variant="body2" color="text.secondary">
                                {props.main_post.frontmatter.excerpt}
                            </Typography>
                        </CardContent>
                    </Box>
                </CardActionArea>
                <CardContent sx={{ width: "100" }}>
                    {/* <Typography gutterBottom variant="h5" component="div">
                            {props.detail_posts.map((detail_post, index) => (
                                <Link
                                    href={`/products/${detail_post.main_foldername}/${detail_post.midd_foldername}/${detail_post.slug}`}
                                >
                                    <dt key={index}>
                                        {detail_post.frontmatter.title}
                                    </dt>
                                </Link>
                            ))}{" "} */}
                    {props.detail_posts.map((detail_post, index) => (
                        <Button
                            variant="contained"
                            component={Link}
                            noLinkStyle
                            href={`/products/${detail_post.main_foldername}/${detail_post.midd_foldername}/${detail_post.slug}`}
                            sx={{ ml: 1, mb: 1 }}
                            key={index}
                        >
                            {detail_post.frontmatter.title}
                        </Button>
                    ))}
                    {/* </Typography> */}
                </CardContent>
            </Card>{" "}
        </Link>
    );
}

// props default vaule
// ProductsCards.defaultProps = {
//     linkto: "/product",
// };
